/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Jon Brule <brulejr@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, switchMap } from 'rxjs/operators';
import * as _ from 'lodash';

import { Goal } from '../goal.model';
import { GoalActionTypes } from '../goal.actions';
import { GoalService } from '../data/goal.service';
import * as actions from '../goal.actions';

@Injectable()
export class GoalEffects {

  constructor(private actions$: Actions,
              private goalService: GoalService) { }

  @Effect() addGoal$ = this.actions$.pipe(
    ofType(GoalActionTypes.AddGoal),
    switchMap((action: actions.AddGoal) => {
      return this.goalService.createGoal(action.payload).pipe(
        map((goal: Goal) => new actions.AddGoalSuccess(goal)),
        catchError(err => of(new actions.AddGoalFailure(err)))
      );
    })
  );

  @Effect() loadGoals$: Observable<Action> = this.actions$.pipe(
    ofType(GoalActionTypes.LoadGoals),
    switchMap((action: actions.LoadGoals) => {
      return this.goalService.loadGoals().pipe(
        map((goals: Goal[]) => new actions.LoadGoalsSuccess(goals)),
        catchError(err => of(new actions.LoadGoalsFailure(err)))
      );
    })
  );

  @Effect() removeGoal$ = this.actions$.pipe(
    ofType(GoalActionTypes.RemoveGoal),
    switchMap((action: actions.RemoveGoal) => {
      return this.goalService.deleteGoal(action.payload).pipe(
        map((goal: Goal) => new actions.RemoveGoalSuccess(goal)),
        catchError(err => of(new actions.RemoveGoalFailure(err)))
      );
    })
  );

}
