/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Jon Brule <brulejr@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import * as _ from 'lodash-uuid';

import { Goal } from '../goal.model';

const HTTP_OPTIONS = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class GoalService {

  private _baseUrl = 'http://localhost:3000/';

  constructor(private _httpClient: HttpClient) { }

  createGoal(goal: Goal): Observable<Object> {
    goal.id = _.uuid();
    goal.timestamp = Math.round(new Date().getTime() / 1000);
    const body = JSON.stringify(goal);
    return this._httpClient.post('/api/goal/', body, HTTP_OPTIONS);
  }

  deleteGoal(goal: Goal): Observable<Object> {
    return this._httpClient.delete('/api/goal/' + goal.id).pipe(
      map(res => goal)
    );
  }

  loadGoals(): Observable<Goal[]> {
    return this._httpClient.get<Goal[]>('/api/goal');
  }

}
