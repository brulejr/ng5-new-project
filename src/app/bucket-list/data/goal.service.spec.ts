/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Jon Brule <brulejr@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
import { TestBed, async, inject } from '@angular/core/testing';
import { HttpClientModule, HttpRequest } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as _ from 'lodash';

import { Goal } from '../goal.model';
import { GoalService } from './goal.service';

describe('GoalService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        HttpClientTestingModule
      ],
      providers: [GoalService]
    });
  });

  it('should be created', inject([GoalService], (service: GoalService) => {
    expect(service).toBeTruthy();
  }));

  it('should load goals', inject([GoalService, HttpTestingController], (service: GoalService, backend: HttpTestingController) => {
    service.loadGoals().subscribe((goals: Goal[]) => {
      expect(goals).toBeDefined();
      expect(goals.length).toBeDefined(2);
      expect(goals[0].name).toBe('GOAL_1');
      expect(goals[1].name).toBe('GOAL_2');
    });
    backend.expectOne({
      url: '/api/goal',
      method: 'GET'
    }).flush([{name: 'GOAL_1'}, {name: 'GOAL_2'}]);
  }));

  it('should create a goal', inject([GoalService, HttpTestingController], (service: GoalService, backend: HttpTestingController) => {
    const goal = <Goal>{
      name: 'GOAL_1'
    };
    service.createGoal(goal).subscribe();
    backend.expectOne((req: HttpRequest<any>) => {
      const body = JSON.parse(req.body);
      return req.url === '/api/goal/'
          && req.method === 'POST'
          && req.headers.get('Content-Type') === 'application/json'
          && body.name === 'GOAL_1'
          && _.isString(body.id);
    }, `POST to 'auth/goal' with goal`);
  }));

  it('should delete a goal', inject([GoalService, HttpTestingController], (service: GoalService, backend: HttpTestingController) => {
    const goal = <Goal>{
      name: 'GOAL_1',
      id: 'ID_1',
      timestamp: 123
    };
    service.deleteGoal(goal).subscribe();
    backend.expectOne((req: HttpRequest<any>) => {
      console.log('*** req', req);
      return req.url === '/api/goal/ID_1'
        && req.method === 'DELETE';
    }, `DELETE to '/api/goal' with goal identifier`);
  }));

});
