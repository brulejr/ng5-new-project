/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Jon Brule <brulejr@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { createFeatureSelector } from '@ngrx/store';

import { Goal } from './goal.model';
import { GoalActionTypes } from './goal.actions';
import * as actions from './goal.actions';

// entity adapter
export const goalAdapter = createEntityAdapter<Goal>();
export interface State extends EntityState<Goal> { }

// default data / initial state
const defaultGoals = {
  ids: [],
  entities: {},
  loading: false,
  loaded: false
};

export const initialState: State = goalAdapter.getInitialState(defaultGoals);

// reducer
export function goalReducer(
  state: State = initialState,
  action: actions.GoalActions
) {
  switch (action.type) {
    case GoalActionTypes.AddGoal:
    case GoalActionTypes.LoadGoals:
    case GoalActionTypes.RemoveGoal: {
      return {
        ...state,
        loading: true,
        loaded: false
      };
    }
    case GoalActionTypes.AddGoalSuccess: {
      return {
        ...goalAdapter.addOne(action.payload, state),
        loading: false,
        loaded: true
      };
    }
    case GoalActionTypes.LoadGoalsSuccess: {
      return {
        ...goalAdapter.addAll(action.payload, state),
        loading: false,
        loaded: true
      };
    }
    case GoalActionTypes.RemoveGoalSuccess: {
      return {
        ...goalAdapter.removeOne(action.payload.id, state),
        loading: false,
        loaded: true
      };
    }
    case GoalActionTypes.AddGoalFailure:
    case GoalActionTypes.LoadGoalsFailure:
    case GoalActionTypes.RemoveGoalFailure: {
      return {
        ...state,
        loading: false,
        loaded: true
      };
    }
    default:
      return state;
  }
}

// create default selectors
export const getGoalState = createFeatureSelector<State>('goal');
export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal
} = goalAdapter.getSelectors(getGoalState);

