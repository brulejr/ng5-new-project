/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Jon Brule <brulejr@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
import { Action } from '@ngrx/store';

import { Goal } from './goal.model';

export enum GoalActionTypes {
  AddGoal = '[BucketList] Add Goal',
  AddGoalSuccess = '[BucketList] Add Goal Success',
  AddGoalFailure = '[BucketList] Add Goal Failure',
  LoadGoals = '[BucketList] Load Goals',
  LoadGoalsSuccess = '[BucketList] Load Goals Success',
  LoadGoalsFailure = '[BucketList] Load Goals Failure',
  RemoveGoal = '[BucketList] Remove Goal',
  RemoveGoalSuccess = '[BucketList] Remove Goal Success',
  RemoveGoalFailure = '[BucketList] Remove Goal Failure'
}

export class AddGoal implements Action {
  readonly type = GoalActionTypes.AddGoal;
  constructor(public payload: Goal) { }
}

export class AddGoalSuccess implements Action {
  readonly type = GoalActionTypes.AddGoalSuccess;
  constructor(public payload: Goal) { }
}

export class AddGoalFailure implements Action {
  readonly type = GoalActionTypes.AddGoalFailure;
  constructor(public payload: any) { }
}

export class LoadGoals implements Action {
  readonly type = GoalActionTypes.LoadGoals;
}

export class LoadGoalsSuccess implements Action {
  readonly type = GoalActionTypes.LoadGoalsSuccess;
  constructor(public payload: Goal[]) { }
}

export class LoadGoalsFailure implements Action {
  readonly type = GoalActionTypes.LoadGoalsFailure;
  constructor(public payload: any) { }
}

export class RemoveGoal implements Action {
  readonly type = GoalActionTypes.RemoveGoal;
  constructor(public payload: Goal) { }
}

export class RemoveGoalSuccess implements Action {
  readonly type = GoalActionTypes.RemoveGoalSuccess;
  constructor(public payload: Goal) { }
}

export class RemoveGoalFailure implements Action {
  readonly type = GoalActionTypes.RemoveGoalFailure;
  constructor(public payload: any) { }
}

export type GoalActions =
  AddGoal
  | AddGoalSuccess
  | AddGoalFailure
  | LoadGoals
  | LoadGoalsSuccess
  | LoadGoalsFailure
  | RemoveGoal
  | RemoveGoalSuccess
  | RemoveGoalFailure;
