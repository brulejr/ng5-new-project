/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Jon Brule <brulejr@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Store, select } from '@ngrx/store';
import { trigger, style, transition, animate, keyframes, query, stagger } from '@angular/animations';

import { AppEvents } from '../../app.events';
import { Goal } from '../../bucket-list';
import { EventService } from '../../core/index';
import * as actions from '../../bucket-list/goal.actions';
import * as fromGoals from '../../bucket-list/goal.reducer';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
  animations: [
    trigger('goals', [
      transition('* => *', [
        query(':enter', style({ opacity: 0 }), {optional: true}),
        query(':enter', stagger('300ms', [
          animate('.6s ease-in', keyframes([
            style({opacity: 0, transform: 'translateY(-75%)', offset: 0}),
            style({opacity: .5, transform: 'translateY(35px)',  offset: 0.3}),
            style({opacity: 1, transform: 'translateY(0)',     offset: 1.0}),
          ]))]), {optional: true}),
        query(':leave', stagger('300ms', [
          animate('.6s ease-out', keyframes([
            style({opacity: 1, transform: 'translateY(0)', offset: 0}),
            style({opacity: .5, transform: 'translateY(35px)',  offset: 0.3}),
            style({opacity: 0, transform: 'translateY(-75%)',     offset: 1.0}),
          ]))]), {optional: true})
      ])
    ])
  ]
})
export class HomePageComponent implements AfterViewInit, OnInit {

  goalText = '';
  goals$: Observable<Goal[]>;
  goalsCount$: Observable<number>;

  constructor(
    private _eventService: EventService,
    private _store: Store<Goal>
  ) { }

  addGoal() {
    if (this.goalText) {
      this._store.dispatch(new actions.AddGoal({ name: this.goalText }));
    }
  }

  ngAfterViewInit(): void {
    this._eventService.post(AppEvents.APP_TITLE, {name: 'page.home.title'});
  }

  ngOnInit(): void {
    this.goals$ = this._store.pipe(select(fromGoals.selectAll));
    this.goalsCount$ = this._store.pipe(select(fromGoals.selectTotal));
    this._store.dispatch(new actions.LoadGoals());
  }

  removeGoal(goal: Goal) {
    this._store.dispatch(new actions.RemoveGoal(goal));
  }

}
