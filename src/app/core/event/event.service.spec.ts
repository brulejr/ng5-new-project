/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Jon Brule <brulejr@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import { TestBed, async, inject } from '@angular/core/testing';

import { EventService } from './event.service';

describe('EventService', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EventService]
    });
  });

  it('should be created', inject([EventService], (service: EventService) => {
    expect(service).toBeTruthy();
  }));

  describe( '#post', () => {

    it('should not fail when posting an unhandled event', inject([EventService], (service: EventService) => {
      service.post('TEST_NO_HANDLERS', {name: 'test.event'});
    }));

    it('should send a message to a matching event handler', done => {
      inject([EventService], (service: EventService) => {
        let count = 2;
        const subscription1 = service.on('TEST1').subscribe((event) => {
          expect(event).toBeDefined();
          expect(event.type).toBe('TEST1');
          expect(event.data).toBeDefined();
          expect(event.data.name).toBe('test.event.1');
          count -= 1;
        });
        console.log('subscription1=', subscription1.id);
        const subscription2 = service.on('TEST2').subscribe((event) => {
          expect(event).toBeDefined();
          expect(event.type).toBe('TEST2');
          expect(event.data).toBeDefined();
          expect(event.data.name).toBe('test.event.2');
          count -= 1;
        });
        console.log('subscription2=', subscription2.id);

        expect(service.post('TEST1', {name: 'test.event.1'})).toBeTruthy();
        expect(service.post('TEST2', {name: 'test.event.2'})).toBeTruthy();

        setTimeout(function(){
          expect(count === 0).toBeTruthy();
          subscription1.unsubscribe();
          subscription2.unsubscribe();
          done(); // call this to finish off the it block
        }, 500);
      })();
    });

    it('should block a message that does not meet a guard condition', done => {
      inject([EventService], (service: EventService) => {
        let count = 2;
        const subscription1 = service.on('TEST').subscribe((event) => {
          expect(event).toBeDefined();
          expect(event.type).toBe('TEST');
          expect(event.data).toBeDefined();
          expect(event.data.name).toBe('test.event.2');
          count -= 1;
        });
        console.log('subscription1=', subscription1.id);
        const subscription2 = service.on('TEST', (event) => false).subscribe((event) => {
          fail();
        });
        console.log('subscription2=', subscription2.id);
        const subscription3 = service.on('TEST').subscribe((event) => {
          expect(event).toBeDefined();
          expect(event.type).toBe('TEST');
          expect(event.data).toBeDefined();
          expect(event.data.name).toBe('test.event.2');
          count -= 1;
        });
        console.log('subscription3=', subscription3.id);

        expect(service.post('TEST', {name: 'test.event.1'})).toBeFalsy();

        subscription2.unsubscribe();
        expect(service.post('TEST', {name: 'test.event.2'})).toBeTruthy();

        setTimeout(function(){
          expect(count === 0).toBeTruthy();
          subscription1.unsubscribe();
          subscription3.unsubscribe();
          done(); // call this to finish off the it block
        }, 500);
      })();
    });
  });

});
