# Overview

This project is based upon the [Creating-and-Using-Angular-5-Services](https://coursetro.com/posts/code/110/Creating-and-Using-Angular-5-Services) tutorial. It extends this introductory tutorial with the following concepts:
* I18N using `ng-translate`
* Central data store using `ngrx-store` and `ngrx-entity`

# Operations

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.6.5.

## Running the Code

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Running the Unit Tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Building the Project

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.
